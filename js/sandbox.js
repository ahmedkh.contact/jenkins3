/*message = "Hey";
alert(message);*/

// TAB de données
users = ["John", "Olivia", "Jack"];
message = "Welcome "+users[1]+" !";

console.log(message)

// Les TAB peuvent se composer de tout types
account=[12345, "Olivia", "Jones", true];

console.log("Numéro : "+account[0]+" ["+ typeof(account[0])+"]");
console.log("Prénom : "+account[1]+" ["+ typeof(account[1])+"]");
console.log("Nom : "+account[2]+" ["+ typeof(account[2])+"]");
console.log("En activité : "+account[3]+" ["+ typeof(account[3])+"]");

// La longueur des TAB
console.log("Longueur TAB : "+account.length); 

// Fonctions
function showDryMessage() {
    console.log("Don't repeat yourself");
}

showDryMessage();
console.log("Another line");
showDryMessage();

// Déclara° de var avec let
let season = "spring";
console.log("Saison : "+season);
season = "summer";
console.log("Saison : "+season);
console.log("********************************************************");

// Décla de constantes avec const

const pi = 3.14159;

function getCircleArea(radius){
    let area = pi * (radius ** 2);
    console.log("Aire du cercle : "+area);
}

getCircleArea(300000);
console.log("********************************************************");

//Portée locale & globale des variables
function showFruit(){
    let fruit = 'Lemon';
    return fruit;
}

let fruit = 'Apple';
console.log('Fruit : '+fruit+' fonction -> '+showFruit());
console.log("********************************************************");

// Passage de tableau lors de l'appel d'une fonction
function showCarData(car){
    return 'Marque : ' + car[0] + '\nModele : ' + car[1] + '\nCarburant : ' + car[2] + '\nDispo : : ' + car[3];
}

let car = ['Toyota', 'Yaris', 'Hybride', true];
console.log(showCarData(car));
console.log("********************************************************");

// Fonction valeur ajoutée
const valTva = 21;

function getAppTva(price){
    let appTva = price + (price / 100) * valTva;
    return appTva;
}

const separe = '\n********************************************************'

let appTva = getAppTva(100);
console.log('Prix TTC : '+ appTva + separe);

// Utilisation de IF
if (2 * 3 == 6) {
    console.log('2 x 3 est bien égal à 6');
}

// Utilisation de TRUE dans IF

let active = true;
if (active){
    rep = 'la valeur est "true"'
}

console.log(rep + separe);

// Utilisation de ELSE
let result = 15;

if (result > 100) {
    console.log('Résultat > que 100');
} else {
    console.log('Résultat > à 100'+separe);
}

// Accéder à tous les liens ' <a> ' de la page
let links = document.getElementsByTagName('a');
console.log(links);
console.log(links[5]);
console.log(links.item[7]);


//      Modifier
let jsDom1 = document.getElementById('jsDom1');
jsDom1.innerHTML = 'Javascript vient de modifier ça';


//      Style
let jsDom2 = document.getElementById('jsDom2');
jsDom2.style.color = 'green';
jsDom2.style.fontSize = '130%';
jsDom2.style.fontWeight = 'bold';


//      Classe CSS
let jsDom3 = document.getElementById('jsDom3');
jsDom3.setAttribute('class','js_dom3');


//      Créer
const jsDom4 = document.createElement('p');
const content4 = document.createTextNode('Temporaire');
jsDom4.appendChild(content4);
const main = document.getElementById('main');
main.appendChild(jsDom4);
jsDom4.innerHTML = 'JavaScript crée des balises';
jsDom4.setAttribute('class','js_dom4');


//      Afficher / Masquer

const toggleBtn = document.createElement('button');
main.appendChild(toggleBtn);
toggleBtn.setAttribute('class','js_dom5');
toggleBtn.innerHTML = 'cacher';
toggleBtn.onclick = function() {hideP('jsDom3')};

function hideP(id){
    let para = document.getElementById(id);
    if (para.style.display === 'none') {
        para.style.display = 'block';
        toggleBtn.innerHTML = 'cacher';}
        else {
            para.style.display = 'none';
            toggleBtn.innerHTML = 'montrer';}}

//      Afficher / Masquer

const toggleBtn = document.createElement('button');
main.appendChild(toggleBtn);
toggleBtn.setAttribute('class','js_dom5');
toggleBtn.innerHTML = 'cacher';
toggleBtn.onclick = function() {hideP('jsDom3')};

function hideP(id){
    let para = document.getElementById(id);
    if (para.style.display === 'none') {
        para.style.display = 'block';
        toggleBtn.innerHTML = 'cacher';}
        else {
            para.style.display = 'none';
            toggleBtn.innerHTML = 'montrer';}}
