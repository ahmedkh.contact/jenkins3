const slider = document.createElement("img");
slider.src = "img/slider1.jpg";
slider.className = "slider_image border_off";
slider.id = "slider_image";
slider.style.cursor = "pointer";
document.getElementById("main").appendChild(slider);

let imgIndex = 1;
const nbrImages = 3;
const temps = 1000;
let interval;

slider.onclick = function() {
if (interval) {
clearInterval(interval);
interval = null;
} else {
interval = setInterval(function() {
slider.src = `img/slider${imgIndex}.jpg`;
if (imgIndex === nbrImages) {
imgIndex = 1;
} else {
imgIndex++;
}
}, temps);
}
};